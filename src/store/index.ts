/* eslint-disable no-console */
import Vue from 'vue';
import Vuex from 'vuex';

import {
  Coordinates,
  MarkerData,
  SharedRaidBoss,
  Status,
} from '@/components/models';
import raidBoss from './modules/raidBoss/raidBoss';
import trainerCode from './modules/trainerCode/trainerCode';
import * as Actions from './actions';
import * as Mutations from './mutations';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    raidBoss,
    trainerCode,
  },
  state: {
    isTrainerCodesPanelOpen: false,
    isSearchMenuPanelOpen: false,
    selectedSharedRaid: null as SharedRaidBoss | null,
    // TODO: combine geolocationByIPStatus and geolocationByHTML5Status into 1?
    geolocationByIPStatus: 'request' || 'success' || 'failure' as Status,
    geolocationByHTML5Status: 'request' || 'success' || 'failure' as Status,
    geolocation: null as Coordinates | null,
  },
  mutations: { // i.e. reducers
    [Mutations.OPEN_TRAINER_CODES_PANEL]: ((state) => {
      state.isTrainerCodesPanelOpen = true;
    }),
    [Mutations.CLOSE_TRAINER_CODES_PANEL]: ((state) => {
      state.isTrainerCodesPanelOpen = false;
    }),
    [Mutations.OPEN_SEARCH_MENU_PANEL]: ((state) => {
      state.isSearchMenuPanelOpen = true;
    }),
    [Mutations.CLOSE_SEARCH_MENU_PANEL]: ((state) => {
      state.isSearchMenuPanelOpen = false;
    }),
    [Mutations.SET_SELECTED_RAID]: ((state, selectedSharedRaid: SharedRaidBoss) => {
      state.selectedSharedRaid = selectedSharedRaid;
    }),
    [Mutations.SET_GEOLOCATION_BY_IP_REQUEST]: ((state) => {
      state.geolocationByIPStatus = 'request';
    }),
    [Mutations.SET_GEOLOCATION_BY_IP_SUCCESS]: ((state, { latitude, longitude }: Coordinates) => {
      state.geolocation = {
        ...state.geolocation,
        latitude,
        longitude,
      };
      state.geolocationByIPStatus = 'success';
    }),
    [Mutations.SET_GEOLOCATION_BY_IP_FAILURE]: ((state) => {
      state.geolocationByIPStatus = 'failure';
    }),
    [Mutations.SET_GEOLOCATION_BY_HTML5_REQUEST]: ((state) => {
      state.geolocationByHTML5Status = 'request';
    }),
    // cannot destructure location: Either<Position> or else TS loses type info
    [Mutations.SET_GEOLOCATION_BY_HTML5_SUCCESS]: ((
      state,
      coordinates: Coordinates,
    ) => {
      state.geolocation = {
        ...state.geolocation,
        ...coordinates,
      };
      state.geolocationByHTML5Status = 'success';
    }),
    [Mutations.SET_GEOLOCATION_BY_HTML5_FAILURE]: ((state) => {
      state.geolocationByHTML5Status = 'failure';
    }),
  },
  actions: { // actions/effects
    [Actions.CLICK_MARKER]({ commit, dispatch }, { data, type }: MarkerData<SharedRaidBoss>) {
      switch (type) {
        case 'raid':
          commit(Mutations.SET_SELECTED_RAID, data);
          break;
        case 'pokestop':
          break;
        case 'teamRocket':
          break;
        default:
          console.error('Error');
          break;
      }
      dispatch(Actions.OPEN_TRAINER_CODES_PANEL);
    },
    [Actions.OPEN_TRAINER_CODES_PANEL](context) {
      context.commit(Mutations.OPEN_TRAINER_CODES_PANEL);
    },
    [Actions.CLOSE_TRAINER_CODES_PANEL](context) {
      context.commit(Mutations.CLOSE_TRAINER_CODES_PANEL);
      context.commit(Mutations.SET_SELECTED_RAID, null);
    },
    [Actions.OPEN_SEARCH_MENU_PANEL](context) {
      context.commit(Mutations.OPEN_SEARCH_MENU_PANEL);
    },
    [Actions.CLOSE_SEARCH_MENU_PANEL](context) {
      context.commit(Mutations.CLOSE_SEARCH_MENU_PANEL);
    },
    async [Actions.GET_GEOLOCATION_BY_HTML5]({ commit }) {
      commit(Mutations.SET_GEOLOCATION_BY_HTML5_REQUEST);

      // wrapper for getCurrentPosition
      const requestGeolocation = (
        options?: PositionOptions,
      ): Promise<Position | PositionError> => new Promise((success, reject) => navigator
        .geolocation.getCurrentPosition(success, reject, options));

      // ask for permission
      try {
        const { coords: { latitude, longitude } } = await requestGeolocation({
          timeout: 10000,
        }) as Position;
        commit(Mutations.SET_GEOLOCATION_BY_HTML5_SUCCESS, { latitude, longitude });
      } catch (err) {
        let errMsg: string;
        switch (err.code) {
          case err.PERMISSION_DENIED:
            errMsg = 'User denied the request for Geolocation.';
            break;
          case err.POSITION_UNAVAILABLE:
            errMsg = 'Location information is unavailable.';
            break;
          case err.TIMEOUT:
            errMsg = 'The request to get user location timed out.';
            break;
          case err.UNKNOWN_ERROR:
            errMsg = 'An unknown error occurred.';
            break;
          default:
            errMsg = '';
        }
        console.error(errMsg);
        commit(Mutations.SET_GEOLOCATION_BY_HTML5_FAILURE);
      }
    },
    async [Actions.GET_GEOLOCATION_BY_IP]({ commit, dispatch }) {
      commit(Mutations.SET_GEOLOCATION_BY_IP_REQUEST);
      try {
        const res: Response = await fetch(
          `${process.env.VUE_APP_API}/geolocation`,
        );
        const body: Coordinates = await res.json();
        commit(Mutations.SET_GEOLOCATION_BY_IP_SUCCESS, body);
      } catch (err) {
        console.error(err);
        commit(Mutations.SET_GEOLOCATION_BY_IP_FAILURE);
        // fallback coordinates
        const coordinates = {
          longitude: -122.420679,
          latitude: 37.772537,
        };
        commit(Mutations.SET_GEOLOCATION_BY_IP_SUCCESS, coordinates);
      } finally {
        // try to get a more accurate location via HTML5
        dispatch(Actions.GET_GEOLOCATION_BY_HTML5);
      }
    },
  },
});
