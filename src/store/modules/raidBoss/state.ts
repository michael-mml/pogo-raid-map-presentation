import { RaidBoss, SharedRaidBoss, Status } from '@/components/models';

export type RaidBossState = {
  raidBossesStatus: Status;
  raidBosses: RaidBoss[];
  sharedRaidBossesStatus: Status;
  sharedRaidBosses: SharedRaidBoss[];
  shareRaidBossStatus: Status;
};

export const state: RaidBossState = {
  raidBossesStatus: 'success' || 'request' || 'failure' as Status,
  raidBosses: [] as RaidBoss[],
  sharedRaidBossesStatus: 'success' || 'request' || 'failure' as Status,
  sharedRaidBosses: [] as SharedRaidBoss[],
  shareRaidBossStatus: 'success' || 'request' || 'failure' as Status,
};
