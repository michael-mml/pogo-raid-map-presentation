import { MutationTree } from 'vuex';
import { RaidBoss, SharedRaidBoss } from '@/components/models';
import * as MutationTypes from './mutationTypes';
import { RaidBossState } from './state';

export default {
  [MutationTypes.SHARE_RAID_BOSS_REQUEST]: ((state: RaidBossState) => {
    state.shareRaidBossStatus = 'request';
  }),
  [MutationTypes.SHARE_RAID_BOSS_SUCCESS]: ((state: RaidBossState) => {
    state.shareRaidBossStatus = 'success';
  }),
  [MutationTypes.SHARE_RAID_BOSS_FAILURE]: ((state: RaidBossState) => {
    state.shareRaidBossStatus = 'failure';
  }),
  [MutationTypes.GET_RAID_BOSSES_REQUEST]: ((state) => {
    state.raidBossesStatus = 'request';
  }),
  [MutationTypes.GET_RAID_BOSSES_SUCCESS]: ((state, payload: RaidBoss[]) => {
    // TODO: figure out why the state only changes with .push() and not with array destructuring
    // for (let boss of payload) {
    //   state.raidBosses.push(boss);
    // }
    state.raidBosses = [...payload];
    state.raidBossesStatus = 'success';
  }),
  [MutationTypes.GET_RAID_BOSSES_FAILURE]: ((state) => {
    state.raidBossesStatus = 'failure';
  }),
  [MutationTypes.GET_SHARED_RAID_BOSSES_REQUEST]: ((state) => {
    state.sharedRaidBossesStatus = 'request';
  }),
  [MutationTypes.GET_SHARED_RAID_BOSSES_SUCCESS]: ((state, payload: SharedRaidBoss[]) => {
    state.sharedRaidBosses = payload;
    state.sharedRaidBossesStatus = 'success';
  }),
  [MutationTypes.GET_SHARED_RAID_BOSSES_FAILURE]: ((state) => {
    state.sharedRaidBossesStatus = 'failure';
  }),
} as MutationTree<RaidBossState>;
