import { Coordinates, RaidBoss, SharedRaidBoss } from '@/components/models';
import { ActionContext } from 'vuex';
import * as ActionTypes from './actionTypes';
import * as MutationTypes from './mutationTypes';
import { RaidBossState } from './state';

export default {
  // TODO: consider re-factoring post to a separate function
  async [ActionTypes.SHARE_RAID_BOSS](
    context: ActionContext<RaidBossState, {}>, payload: SharedRaidBoss,
  ) {
    // TODO: attempt to save the share details if it fails
    // const savedShare = [...context.statestate];
    context.commit(MutationTypes.SHARE_RAID_BOSS_REQUEST);
    try {
      const res: Response = await fetch(
        `${process.env.VUE_APP_API}/raid_boss`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(payload),
        },
      );
      await res.json();
      context.commit(MutationTypes.SHARE_RAID_BOSS_SUCCESS);

      // update the map with newly shared raid boss
      context.dispatch(ActionTypes.GET_SHARED_RAID_BOSSES, payload.location);
    } catch (err) {
      console.error(err);
      context.commit(MutationTypes.SHARE_RAID_BOSS_FAILURE);
    }
  },
  async [ActionTypes.GET_RAID_BOSSES](context: ActionContext<RaidBossState, {}>) {
    context.commit(MutationTypes.GET_RAID_BOSSES_REQUEST);
    try {
      const res: Response = await fetch(
        `${process.env.VUE_APP_API}/raid_bosses`,
      );
      const data: RaidBoss[] = await res.json();
      context.commit(MutationTypes.GET_RAID_BOSSES_SUCCESS, data);
    } catch (err) {
      console.error(err);
      context.commit(MutationTypes.GET_RAID_BOSSES_FAILURE);
    }
  },
  async [ActionTypes.GET_SHARED_RAID_BOSSES](
    context: ActionContext<RaidBossState, {}>, payload: Coordinates,
  ) {
    context.commit(MutationTypes.GET_SHARED_RAID_BOSSES_REQUEST);
    try {
      const res: Response = await fetch(
        `${process.env.VUE_APP_API}/raid_boss/nearby?longitude=${payload.longitude}&latitude=${payload.latitude}`,
      );
      const data: SharedRaidBoss[] = await res.json();

      // if shared raid bosses did not change, do not mutate the state
      let newRaidBosses: SharedRaidBoss[] = context.state.sharedRaidBosses;
      if (data.length !== context.state.sharedRaidBosses.length) {
        newRaidBosses = data;
      } else {
        const prevBosses = data.map(({ raidID }) => raidID).sort();
        const currBosses = context.state.sharedRaidBosses.map(({ raidID }) => raidID).sort();
        const areSharedRaidBossesDiff = prevBosses.some((id, index) => id !== currBosses[index]);
        if (areSharedRaidBossesDiff) {
          newRaidBosses = data;
        }
      }
      context.commit(MutationTypes.GET_SHARED_RAID_BOSSES_SUCCESS, newRaidBosses);
    } catch (err) {
      console.error(err);
      context.commit(MutationTypes.GET_SHARED_RAID_BOSSES_FAILURE);
    }
  },
};
