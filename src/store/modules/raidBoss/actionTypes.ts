export const raidBossModule = 'raidBoss';

export const GET_RAID_BOSSES = 'GET_RAID_BOSSES';

export const SHARE_RAID_BOSS = 'SHARE_RAID_BOSS';

export const GET_SHARED_RAID_BOSSES = 'GET_SHARED_RAID_BOSSES';
