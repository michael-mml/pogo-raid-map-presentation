import { SharedTrainerCode } from '@/components/models';
import { MutationTree } from 'vuex';
import * as MutationTypes from './mutationTypes';
import { TrainerCodeState } from './state';

export default {
  // state is the module's local state
  [MutationTypes.SHARE_TRAINER_CODE_REQUEST]: ((state: TrainerCodeState) => {
    state.shareTrainerCodeStatus = 'request';
  }),
  [MutationTypes.SHARE_TRAINER_CODE_SUCCESS]: ((state: TrainerCodeState) => {
    state.shareTrainerCodeStatus = 'success';
  }),
  [MutationTypes.SHARE_TRAINER_CODE_FAILURE]: ((state: TrainerCodeState) => {
    state.shareTrainerCodeStatus = 'failure';
  }),
  [MutationTypes.GET_TRAINER_CODES_REQUEST]: ((state) => {
    state.trainerCodesStatus = 'request';
  }),
  [MutationTypes.GET_TRAINER_CODES_SUCCESS]: ((state, payload: SharedTrainerCode[]) => {
    state.trainerCodes = [...payload];
    state.trainerCodesStatus = 'success';
  }),
  [MutationTypes.GET_TRAINER_CODES_FAILURE]: ((state) => {
    state.trainerCodesStatus = 'failure';
  }),
} as MutationTree<TrainerCodeState>;
