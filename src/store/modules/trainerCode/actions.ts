import {
  Commit, Dispatch,
} from 'vuex';

import { SharedTrainerCode } from '@/components/models';
import * as ActionTypes from './actionTypes';
import * as MutationTypes from './mutationTypes';

export default {
  async [ActionTypes.GET_TRAINER_CODES]({ commit }: { commit: Commit }, raidID: string) {
    commit(MutationTypes.GET_TRAINER_CODES_REQUEST);
    try {
      const res: Response = await fetch(
        `${process.env.VUE_APP_API}/trainer_codes?raidID=${raidID}`,
      );
      const data: SharedTrainerCode[] = await res.json();
      commit(
        MutationTypes.GET_TRAINER_CODES_SUCCESS,
        data,
      );
    } catch (err) {
      console.error(err);
      commit(MutationTypes.GET_TRAINER_CODES_FAILURE);
    }
  },
  async [ActionTypes.SHARE_TRAINER_CODE](
    { commit, dispatch }: { commit: Commit; dispatch: Dispatch }, payload: SharedTrainerCode,
  ) {
    commit(MutationTypes.SHARE_TRAINER_CODE_REQUEST);
    try {
      const res: Response = await fetch(
        `${process.env.VUE_APP_API}/trainer_codes`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(payload),
        },
      );
      await res.json();
      commit(MutationTypes.SHARE_TRAINER_CODE_SUCCESS);

      // update the list of trainer codes with newly shared code
      dispatch(ActionTypes.GET_TRAINER_CODES, payload.raidID);
    } catch (err) {
      console.error(err);
      commit(MutationTypes.SHARE_TRAINER_CODE_FAILURE);
    }
  },
};
