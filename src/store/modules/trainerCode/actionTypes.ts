export const trainerCodeModule = 'trainerCode';

export const SHARE_TRAINER_CODE = 'SHARE_TRAINER_CODE';

export const GET_TRAINER_CODES = 'GET_TRAINER_CODES';
