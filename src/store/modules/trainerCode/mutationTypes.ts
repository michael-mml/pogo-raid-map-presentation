export const SHARE_TRAINER_CODE_REQUEST = 'SHARE_TRAINER_CODE_REQUEST';
export const SHARE_TRAINER_CODE_SUCCESS = 'SHARE_TRAINER_CODE_SUCCESS';
export const SHARE_TRAINER_CODE_FAILURE = 'SHARE_TRAINER_CODE_FAILURE';

export const GET_TRAINER_CODES_REQUEST = 'GET_TRAINER_CODES_REQUEST';
export const GET_TRAINER_CODES_SUCCESS = 'GET_TRAINER_CODES_SUCCESS';
export const GET_TRAINER_CODES_FAILURE = 'GET_TRAINER_CODES_FAILURE';
