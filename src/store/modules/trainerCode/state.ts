import { SharedTrainerCode, Status } from '@/components/models';

export type TrainerCodeState = {
  shareTrainerCodeStatus: Status;
  trainerCodesStatus: Status;
  trainerCodes: SharedTrainerCode[];
};

export const state: TrainerCodeState = {
  shareTrainerCodeStatus: 'success' || 'request' || 'failure' as Status,
  trainerCodesStatus: 'success' || 'request' || 'failure' as Status,
  trainerCodes: [] as SharedTrainerCode[],
};
