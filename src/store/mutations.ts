export const OPEN_TRAINER_CODES_PANEL = 'OPEN_TRAINER_CODES_PANEL';
export const CLOSE_TRAINER_CODES_PANEL = 'CLOSE_TRAINER_CODES_PANEL';

export const OPEN_SEARCH_MENU_PANEL = 'OPEN_SEARCH_MENU_PANEL';
export const CLOSE_SEARCH_MENU_PANEL = 'CLOSE_SEARCH_MENU_PANEL';

export const SET_SELECTED_RAID = 'SET_SELECTED_RAID';

export const SET_GEOLOCATION_BY_IP_REQUEST = 'SET_GEOLOCATION_BY_IP_REQUEST';
export const SET_GEOLOCATION_BY_IP_SUCCESS = 'SET_GEOLOCATION_BY_IP_SUCCESS';
export const SET_GEOLOCATION_BY_IP_FAILURE = 'SET_GEOLOCATION_BY_IP_FAILURE';

export const SET_GEOLOCATION_BY_HTML5_REQUEST = 'SET_GEOLOCATION_BY_HTML5_REQUEST';
export const SET_GEOLOCATION_BY_HTML5_SUCCESS = 'SET_GEOLOCATION_BY_HTML5_SUCCESS';
export const SET_GEOLOCATION_BY_HTML5_FAILURE = 'SET_GEOLOCATION_BY_HTML5_FAILURE';
