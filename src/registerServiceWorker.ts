/* eslint-disable no-console */
const ns = '[registerServiceWorker]';
const serviceWorker = 'serviceWorker.js';

const isLocalhost = Boolean(
  window.location.hostname === 'localhost'
  // [::1] is the IPv6 localhost address.
  || window.location.hostname === '[::1]'
  // 127.0.0.0/8 are considered localhost for IPv4.
  || window.location.hostname.match(
    /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/,
  ),
);

async function registerValidSW(swUrl: string) {
  const fn = `[registerValidSW][${swUrl}]`;
  let registration: ServiceWorkerRegistration;
  try {
    console.info(`${ns}${fn} registering ${swUrl}`);
    registration = await navigator.serviceWorker.register(swUrl);
  } catch (err) {
    console.error(`${ns}${fn} Error during service worker registration: ${err}`);
    return;
  }
  // updatefound is emitted if a SW is installing, we can get a reference to it
  registration.onupdatefound = () => {
    console.log(`${ns}${fn} the service worker's state has changed`);
    const installingWorker = registration.installing;

    // make sure we are listening to the installing SW (and not an active SW)
    if (installingWorker) {
      // statechange is emitted if a SW is finished installing
      installingWorker.onstatechange = () => {
        switch (installingWorker.state) {
          case 'installed':
            // TODO: if the user is idle, dispatch an action to open the popup
            break;
          case 'activated':
            break;
          default:
            break;
        }
      };
    }
  };
  // controllerchange is emitted when a SW is activated (i.e. the SW calls skipWaiting)
  // and takes control of the page
  navigator.serviceWorker.oncontrollerchange = () => {
    console.log(`${ns}${fn}[oncontrollerchange] new worker took control`);
    window.location.reload();
  };

  // TODO: subscribe to the store and pass state listeners
  setInterval(async () => {
    console.log(`${ns}${fn} checking for new service workers`);
    const reg = await navigator.serviceWorker.ready;
    reg.update();

    // TODO: dispatch an action to subscribe to popups (so we stop ignoring popups)

    // if the user went idle after closing the popup and there's a waiting SW
    // prompt the user to refresh the page
    // TODO: if there is a waiting SW and the user is idle
    // dispatch an action to open the popup
  }, 6000000);
}

async function checkValidServiceWorker(swUrl: string) {
  // Check if the service worker can be found. If it can't reload the page.
  try {
    const response = await fetch(swUrl, {
      headers: { 'Service-Worker': 'script' },
    });
    const contentType = response.headers.get('content-type');
    if (
      response.status === 404
      || (contentType != null && contentType.indexOf('javascript') === -1)
    ) {
      // No service worker found. Probably a different app. Reload the page.
      const registration = await navigator.serviceWorker.ready;
      registration.unregister().then(() => {
        window.location.reload();
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl);
    }
  } catch (err) {
    console.log(
      'No internet connection found. App is running in offline mode.',
    );
  }
}

export function register() {
  if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
    // The URL constructor is available in all browsers that support SW.
    const publicUrl = new URL(process.env.BASE_URL, window.location.href);
    if (publicUrl.origin !== window.location.origin) {
      // Our service worker won't work if PUBLIC_URL is on a different origin
      // from what our page is served on. This might happen if a CDN is used to
      // serve assets; see https://github.com/facebook/create-react-app/issues/2374
      return;
    }

    window.addEventListener('load', () => {
      const swUrl = `${serviceWorker}`;

      if (isLocalhost) {
        // This is running on localhost. Let's check if a service worker still exists or not.
        checkValidServiceWorker(swUrl);

        // Add some additional logging to localhost, pointing developers to the
        // service worker/PWA documentation.
        navigator.serviceWorker.ready.then(() => {
          console.log(
            'This web app is being served cache-first by a service '
            + 'worker. To learn more, visit https://bit.ly/CRA-PWA',
          );
        });
      } else {
        // Is not localhost. Just register service worker
        registerValidSW(swUrl);
      }
    });
  }
}

export async function unregister() {
  if ('serviceWorker' in navigator) {
    try {
      const registration = await navigator.serviceWorker.ready;
      registration.unregister();
      // unsubscribe from store
    } catch (error) {
      console.error(error.message);
    }
  }
}
