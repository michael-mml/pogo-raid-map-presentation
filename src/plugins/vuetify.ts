import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  /* https://vuetifyjs.com/en/customization/icons/#install-material-design-icons-js-svg */
  icons: {
    iconfont: 'mdiSvg',
  },
});
