import 'vue-class-component/hooks'; // import hooks type to enable auto-complete
import Vue from 'vue';
import App from './App.vue';
import {
  register,
} from './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

/* Template filters: https://vuejs.org/v2/guide/filters.html */
Vue.filter('titlecase', (value: string) => ((!value || typeof value !== 'string')
  ? ''
  : value
    .split(' ')
    .map((el) => el.charAt(0).toUpperCase() + el.slice(1))
    .join(' ')
));
Vue.filter('hhMm', (iso8601: string, hourFormat: 12 | 24) => ((!iso8601 || typeof iso8601 !== 'string')
  ? ''
  // converts ISO8601 string to HH:MM AM/PM format
  : new Date(Date.parse(iso8601))
    .toLocaleTimeString(
      // use default locale
      [],
      {
        // removes leading 0 if single digit hours
        hour: 'numeric',
        minute: '2-digit',
        hour12: hourFormat === 12,
      },
    )
));
// Pads a number trainer code with leading zeroes
Vue.filter('clipboardTrainerCode', (trainerCode: string) => ((!trainerCode || typeof trainerCode !== 'string')
  ? ''
  : trainerCode.padStart(12, '0')
));
// Inserts spaces into the padded trainer code
Vue.filter('formattedTrainerCode', (trainerCode: string) => ((!trainerCode || typeof trainerCode !== 'string')
  ? ''
  : trainerCode.replace(/(\d{4})(?!$)/g, '$1 ')
));
// Pads a Pokemon number to 3 digits number trainer code with leading zeroes
Vue.filter('pokemonNumber', (pokemonNumber: string) => ((!pokemonNumber || typeof pokemonNumber !== 'string')
  ? ''
  : pokemonNumber.padStart(3, '0')
));

new Vue({
  router,
  store,
  vuetify,
  created: () => {
    // if 404 page was accessed, try to match and redirect to the route prior to the 404 page
    if (sessionStorage.redirect) {
      const { redirect } = sessionStorage;
      delete sessionStorage.redirect;
      router.push(redirect);
    }
  },
  render: (h) => h(App),
}).$mount('#app');

register();
