import Vue from 'vue';
import VueRouter, {
  Route,
  RouteConfig,
} from 'vue-router';

import {
  isShareTargetValid,
} from './validator';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  // TODO: utilize this route for showing a login/authentication page before showing the map
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "gomap" */ '@/views/GoMap.vue'),
    beforeEnter: (to: Route, from: Route, next) => {
      // if we get a share action from another app, to.query is non-null
      // NOTE: share action will always hit the root route i.e. /
      if (to.query.title || to.query.text || to.query.url) {
        if (isShareTargetValid(to)) {
          // re-direct to the nearby route while passing the shared action
          next({
            path: '/nearby', // TODO: consider redirecting to share page
            query: {
              title: to.query.title,
              text: to.query.text,
              url: to.query.url,
            },
          });
        } else {
          // otherwise, redirect to nearby route without passing a query
          next({
            path: '/nearby',
          });
        }
      } else {
        next({
          path: '/nearby',
        });
      }
    },
  },
  {
    path: '/nearby',
    name: 'raid',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "gomap" */ '@/views/GoMap.vue'),
    // DEBUG: this navigation guard is only activated for local development because it is not
    // possible to access this route directly in a production environment
    beforeEnter: (to: Route, from: Route, next) => {
      // remove invalid query params
      if ((to.query.title || to.query.text || to.query.url) && !isShareTargetValid(to)) {
        next({
          path: '/nearby',
          query: {},
        });
      } else {
        next();
      }
    },
    props: (route: Route) => {
      if (route.query.title || route.query.text || route.query.url) {
        // if a valid query param exists, then a share action was made
        // build the share data and pass it as a prop to GoMap.vue
        if (isShareTargetValid(route)) {
          return {
            shareTarget: {
              title: route.query.title,
              text: route.query.text,
              url: route.query.url,
            },
          };
        }
      }
      // otherwise, no prop is passed to GoMap.vue
      return undefined;
    },
  },
  // TODO: consider adding a share component if multiple share locations is implemented
  // {
  //   path: '/share',
  //   name: 'share',
  //   route level code-splitting
  //   this generates a separate chunk (about.[hash].js) for this route
  //   which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ './views/GoMap.vue'),
  //   props: (route: Route) => ({
  //     title: route.query.title,
  //     text: route.query.text,
  //     url: route.query.url,
  //   }),
  // },
  // TODO: this route is used as a quick fix to address PWA loading if the
  // start_url is set to index.html
  {
    path: '/index.html',
    redirect: '/',
  },
  {
    // NOTE: this 'ALL' route must be the last route
    path: '*',
    // TODO: remove this hack when hosting on an actual webserver; used only for GitLab Pages
    name: 'e404',
    component: {
      name: 'e404',
      template: '<h1>404 Page Not Found!</h1>',
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
