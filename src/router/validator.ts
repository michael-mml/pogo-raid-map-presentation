/* eslint-disable import/prefer-default-export */

import { Coordinates } from '@/components/models';
import { Route } from 'vue-router';

/**
 * TODO: refactor with the function in ShareRaidDialog
 */
const convertDMSToDD = (
  degrees: string,
  minutes: string,
  seconds: string,
  direction: string,
) => (direction === 'S' || direction === 'W'
  ? (parseInt(degrees, 10)
      + parseInt(minutes, 10) / 60
      + parseInt(seconds, 10) / (60 * 60))
    * -1
  : parseInt(degrees, 10)
    + parseInt(minutes, 10) / 60
    + parseInt(seconds, 10) / (60 * 60));

/**
 * Returns true if the title contains coordinates. Returns false otherwise.
 *
 * TODO: verify if all shares contain the longitude and latitude.
 *
 * @remarks
 *
 * A valid example: https://michael-mml.gitlab.io/pogo-raid-map-presentation/nearby?title=43%C2%B048%2753.4%22N%2079%C2%B019%2713.1%22W&text=43%C2%B048%2753.4%22N%2079%C2%B019%2713.1%22W%0AMilliken,%20Scarborough,%20ON%0Ahttps%3A%2F%2Fgoo.gl%2Fmaps%2FE4kfRz4DkwiJYv5JA
 *
 * An invalid example: https://michael-mml.gitlab.io/pogo-raid-map-presentation/?title=Dr.%20Norman%20Bethune%20Collegiate%20Institute&text=Dr.%20Norman%20Bethune%20Collegiate%20Institute%0A200%20Fundy%20Bay%20Blvd,%20Scarborough,%20ON%20M1W%203G1%0A%28416%29%20396-8200%0Ahttps%3A%2F%2Fmaps.app.goo.gl%2Fq8uVsFeQFcJkoqGb8
 */
const isShareTargetValid = ({ query: { title } }: Route) => {
  if (!title || typeof title !== 'string') {
    return false;
  }
  // try to convert query param into coordinates
  const dms: string[] = (title as string)?.split(/[^\d\w]+/);
  const coordinates: Coordinates = {
    longitude: convertDMSToDD(dms[5], dms[6], dms[7], dms[9]),
    latitude: convertDMSToDD(dms[0], dms[1], dms[2], dms[4]),
  };
  // query param did not contain coordinates => invalid share
  return Object.values(coordinates).every((el) => !Number.isNaN(el));
};

export {
  isShareTargetValid,
};
