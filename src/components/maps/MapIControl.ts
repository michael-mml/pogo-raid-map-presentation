import {
  IControl,
  Map as MapboxGLMap,
} from 'mapbox-gl';
import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class MapIControl extends Vue implements IControl {
  map: MapboxGLMap | null = null;

  // is received as props from the Vue mapControl component
  position!: 'top-right' | 'top-left' | 'bottom-right' | 'bottom-left';

  mounted(): void {
    this.$nextTick(() => {
      // TODO: dependency injection for the map component
      this.map = ((this.$parent as any).map as MapboxGLMap);
      // TODO: potential race condition with v-if rendering of Map component
      this.map.addControl(
        this,
        this.position as 'top-right' | 'top-left' | 'bottom-right' | 'bottom-left',
      );
    });
  }

  beforeDestroy(): void {
    // if the component is removed from the DOM
    // remove reference to the map and remove the control from the map
    if (this.map) {
      this.map.removeControl(this);
      this.map = null;
    }
  }

  /**
   * Called when `map.addControl(...)` is called.
   */
  onAdd(map: MapboxGLMap): HTMLElement {
    this.map = map;
    return this.$el as HTMLElement;
  }

  /**
   * Called when `map.removeControl(...)` is called.
   */
  onRemove(): void {
    // this.$el refers to template in the component that implements this class
    if (this.$el.parentNode) {
      // parentNode refers to the MapboxGL element that houses the controls e.g.
      // <div class="mapboxgl-ctrl-top-right">
      this.$el.parentNode.removeChild(this.$el);
    }
    this.map = null;
    // release resources
    this.$destroy();
  }
}
