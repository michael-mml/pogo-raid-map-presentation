type Left = {
  tag: 'left';
  result: 'error';
};
type Right<T> = {
  tag: 'right';
  result: T;
};
export type Either<T> = Left | Right<T>;

export interface Shareable {
  title: string;
  text: string;
  url: string;
}

export type RaidTier = '1' | '3' | '5' | 'mega';

export interface RaidBoss {
  id: number;
  name: string | 'unknown';
  number: number;
  tier: RaidTier;
}

export type Coordinates = {
  longitude: number;
  latitude: number;
}

export type SharedRaidBoss = {
  raidID: string;
  raidBossID: number;
  raidBossName: string | null;
  raidBossCanonicalName?: string; // used to map to pokemon icons, but not required to share a boss
  // number is returned after a boss is shared, but not required to share a boss
  raidBossNumber?: number;
  raidBossTier: RaidTier;
  hatchUTCTimestamp: string;
  location: Coordinates;
  userStatus: boolean; // true = going, false = reporting only
}

export type SharedTrainerCode = {
  shareCodeID: string;
  raidID: string;
  trainerCode: number;
}

export type PopupData = {
  raidBossName: string;
  raidBossTier: RaidTier;
  hatchUTCTimestamp: string;
}

export type Status = 'success' | 'request' | 'failure';

export type MarkerType = 'raid' | 'pokestop' | 'teamRocket';

export type MarkerData<T> = {
  data: T;
  type: MarkerType;
  event: Event;
}

export type VForm = Vue & { validate: () => boolean }; // typing for v-form: https://stackoverflow.com/a/52109899
