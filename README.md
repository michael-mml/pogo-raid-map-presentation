# pogo-raid-map-presentation

Production: [Pokemon Go Raid Map](https://michael.mml.gitlab.io/pogo-raid-map-presentation/nearby)

## Issues

### mapbox-gl-js

- [mapboxgl height initialization:](https://github.com/mapbox/mapbox-gl-js/issues/3265)
- [mapboxgl has no typing](https://stackoverflow.com/questions/44332290/mapbox-gl-typing-wont-allow-accesstoken-assignment)

### vue-router

- [vue-router history mode](https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations)

## Getting Started

Make sure you set the `VUE_APP_MAPBOX_TOKEN` environment variable in your shell before developing. Otherwise, you may see a blank map.

### GitLab CI

[Integrating Yarn with GitLab Ci](https://yarnpkg.com/lang/en/docs/install-ci/)

[Writing a .gitlab-ci.yml for deploying to GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html)

### Vue CLI

#### CSS

[To add/remove a pre/post-processor:](https://cli.vuejs.org/guide/css.html#pre-processors) `yarn add/remove sass/less/stylus-loader sass/less/stylus`

### Vue

#### TypeScript

Gotchas: casting must be done using the `this.el as X` syntax rather than `<X>this.el`

#### Components

Think of Vue components under `src/views` as sections in a page. Each component can be used as part of a larger Vue component view.

##### Computed properties

- can have a `get` and `set` (extremely useful for handling `v-model`, see commit [`2a4a94e883a50fe1decf405568c3daecc2c6754e`](https://gitlab.com/michael-mml/pogo-raid-map-presentation/merge_requests/9/diffs?commit_id=2a4a94e883a50fe1decf405568c3daecc2c6754e))

#### Views

Think of Vue components under `src/views` as pages. The user is routed to various Vue components under `src/views`. Those components can be composed of multiple components.

### Vuetify

`App.vue` (or wherever the starting point of the app) must have `<v-app></v-app>` as the direct child of the template. This registers Vuetify throughout the app.

```vue
<template>
  <v-app id="app">
    <v-navigation-drawer id="nav">
    </v-navigation-drawer>
    <v-main>
      <v-container fluid>
        <router-view />
      </v-container>
    </v-main>
  </v-app>
</template>
```

#### Getting started

Add Vuetify via `vue add vuetify`.

If using in a TypeScript project and you encounter [`Could not find a declaration file for module 'vuetify/lib'`](https://vuetifyjs.com/en/introduction/frequently-asked-questions/#when-adding-typescript-error-could-not-find-a-declaration-file-for-module-vuetify-lib), add `vuetify` to `compilerOptions.types` in [`tsconfig.json`](https://www.typescriptlang.org/tsconfig#types).

### VueX

- mutations must be synchronous => API calls must be done in actions

---

## Project setup

```bash
yarn install
```

### Compiles and hot-reloads for development

```bash
yarn serve
```

### Compiles and minifies for production

```bash
yarn build
```

### Run your unit tests

```bash
yarn test:unit
```

### Run your end-to-end tests

```bash
yarn test:e2e
```

### Lints and fixes files

```bash
yarn lint
```

### Run and test with service workers enabled

Ensure `python3` is installed on your machine.

```bash
yarn serve:sw
```

This builds the app in `service-worker` mode, which enables service workers but does not point redirect to [`/${process.env.CI_PROJECT_NAME}/`](vue.config.js).

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

---

## Deployment

### GitLab Pages

<https://cli.vuejs.org/guide/deployment.html#gitlab-pages>

## Credits

Icons: <https://github.com/TheArtificial/pokemon-icons>
Pokemon Go Assets: <https://github.com/ZeChrales/PogoAssets>
