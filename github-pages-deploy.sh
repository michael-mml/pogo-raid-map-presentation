# For Github Pages deployments
#!/usr/bin/env sh

# abort on errors
set -e

# toggle flag to set publicPath = '/'
export VUE_APP_GITHUB_PAGES=true

# build
yarn build

# navigate into the build output directory
cd dist

# ignore sourcemaps when uploading
echo "js/*.map" >> .gitignore

git init
git add -A
git commit -m 'deploy'

git push -f https://github.com/michael-mml/michael-mml.github.io.git master:gh-pages

cd -
