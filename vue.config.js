/* eslint-disable @typescript-eslint/camelcase */

module.exports = {
  configureWebpack: {
    optimization: {
      // create more, smaller chunked dependencies (<= 250KB)
      // https://stackoverflow.com/q/51816020
      splitChunks: {
        minSize: 10000,
        maxSize: 250000,
      },
    },
  },
  /* https://cli.vuejs.org/guide/deployment.html#gitlab-pages */
  // Files in the public folder are copied directly into dist/
  publicPath: process.env.NODE_ENV === 'production' && !process.env.VUE_APP_LOCAL_SW && !process.env.VUE_APP_GITHUB_PAGES
    ? `/${process.env.CI_PROJECT_NAME}/`
    : '/',
  /* https://cli.vuejs.org/core-plugins/pwa.html#vue-cli-plugin-pwa */
  pwa: {
    // Will generate a manifest.json from the options below + default options
    manifestOptions: {
      /* https://web.dev/web-share-target/#accepting-basic-information */
      share_target: {
        // TODO: remove /pogo-raid-map-presentation/ when the site is moved off of GitLab Pages
        action: '/pogo-raid-map-presentation/',
        method: 'GET',
        enctype: 'application/x-www-form-urlencoded',
        params: {
          title: 'title',
          text: 'text',
          url: 'url',
        },
      },
    },
    /* https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin#injectmanifest */
    // Use a custom service worker instead of a pre-generated service worker
    workboxPluginMode: 'InjectManifest',
    /* https://developers.google.com/web/tools/workbox/reference-docs/latest/module-workbox-webpack-plugin.InjectManifest#InjectManifest */
    workboxOptions: {
      // Point to location of the custom service worker
      // TODO: When Workbox is updated to v5, cache index.html using process.env.BASE_URL
      // Workbox v5 allows Webpack to compile the service worker so we can use process.env.BASE_URL
      // To do this, include it in src/ so it can be picked up by entry point: https://webpack.js.org/concepts/#entry
      // Webpack will only compile and copy serviceWorker.js into dist/ if NODE_ENV=production: https://cli.vuejs.org/core-plugins/pwa.html#vue-cli-plugin-pwa
      swSrc: 'src/serviceWorker.js',
    },
  },
  transpileDependencies: [
    'vuetify',
  ],
};
